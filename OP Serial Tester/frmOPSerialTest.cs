﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OP_Serial_Tester
{
    public partial class frmOPSerialTest : Form
    {
        public frmOPSerialTest()
        {
            InitializeComponent();
        }

        delegate void SetTextCallback(string str_Data);

        private void btnWriteData_Click(object sender, EventArgs e)
        {
            if (sp_GageData.IsOpen == false)
            {
                this.SaveSetup();
                this.sp_GageData.Open();
            }

            sp_GageData.Write(this.txtWriteData.Text);
            return;
        }

        private void btnConnectSetup_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.sp_GageData.IsOpen)
                {
                    this.sp_GageData.Close();
                }

                this.SaveSetup();
                this.sp_GageData.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }
            if (this.sp_GageData.IsOpen)
            {
                MessageBox.Show("You are connected on: " + sp_GageData.PortName);
            }

            return;
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            if (this.sp_GageData.IsOpen)
            {
                this.sp_GageData.Close();
            }
            return;
        }

        void frmOPSerialTest_Load(object sender, System.EventArgs e)
        {
        }

        void sp_GageData_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            string str_SerialData = this.sp_GageData.ReadLine();
            string str_ManipulatdData = str_SerialData;
            SetReceiveDataText(str_ManipulatdData);
            return;
        }

        public void SetReceiveDataText(string str_Data)
        {
            if (this.txtReceiveData.InvokeRequired)
            {
                SetTextCallback tcb_CBData = new SetTextCallback(SetReceiveDataText);
                this.Invoke(tcb_CBData, new object[] { str_Data });
            }
            else
            {
                if (this.txtReceiveData.Multiline)
                {
                    this.txtReceiveData.Text += str_Data + "\r\n";
                }
                else
                {
                    this.txtReceiveData.Text = str_Data;
                }
            }
            return;
        }

        private void SaveSetup()
        {
            this.sp_GageData.PortName = "COM" + iptCOMPort.Value;
            this.sp_GageData.BaudRate = Convert.ToInt32(iptBaudRate.Text);
            this.sp_GageData.DataBits = Convert.ToInt32(iptDataBits.Text);
            switch (iptParity.ValueMember)
            {
                case "None":
                    this.sp_GageData.Parity = System.IO.Ports.Parity.None;
                    break;
                case "Odd":
                    this.sp_GageData.Parity = System.IO.Ports.Parity.Odd;
                    break;
                case "Even":
                    this.sp_GageData.Parity = System.IO.Ports.Parity.Even;
                    break;
                case "Mark":
                    this.sp_GageData.Parity = System.IO.Ports.Parity.Mark;
                    break;
                case "Space":
                    this.sp_GageData.Parity = System.IO.Ports.Parity.Space;
                    break;
                default:
                    this.sp_GageData.Parity = System.IO.Ports.Parity.None;
                    break;
            }
            switch (iptStopBits.ValueMember)
            {
                case "None":
                    this.sp_GageData.StopBits = System.IO.Ports.StopBits.None;
                    break;
                case "One":
                    this.sp_GageData.StopBits = System.IO.Ports.StopBits.One;
                    break;
                case "OnePointFive":
                    this.sp_GageData.StopBits = System.IO.Ports.StopBits.OnePointFive;
                    break;
                case "Two":
                    this.sp_GageData.StopBits = System.IO.Ports.StopBits.Two;
                    break;
                default:
                    this.sp_GageData.StopBits = System.IO.Ports.StopBits.One;
                    break;
            }
            switch (iptHandshake.ValueMember)
            {
                case "None":
                    this.sp_GageData.Handshake = System.IO.Ports.Handshake.None;
                    break;
                case "XOnXOff":
                    this.sp_GageData.Handshake = System.IO.Ports.Handshake.XOnXOff;
                    break;
                case "RequestToSend":
                    this.sp_GageData.Handshake = System.IO.Ports.Handshake.RequestToSend;
                    break;
                case "RequestToSendXOnXOff":
                    this.sp_GageData.Handshake = System.IO.Ports.Handshake.RequestToSendXOnXOff;
                    break;
                default:
                    this.sp_GageData.Handshake = System.IO.Ports.Handshake.None;
                    break;
            }
            switch (iptNewLine.Text)
            {
                case "CR":
                    this.sp_GageData.NewLine = "\r";
                    break;
                case "LF":
                    this.sp_GageData.NewLine = "\n";
                    break;
                case "CRLF":
                    this.sp_GageData.NewLine = "\r\n";
                    break;
                default:
                    this.sp_GageData.NewLine = "\r";
                    break;
            }
            this.sp_GageData.DtrEnable = iptDTREnable.Checked;
            this.sp_GageData.RtsEnable = iptRTSEnable.Checked;
            this.sp_GageData.ReadBufferSize = Convert.ToInt32(iptReadBuffer.Text);
            this.sp_GageData.ReadTimeout = Convert.ToInt32(iptReadTimeout.Text);
            this.sp_GageData.WriteBufferSize = Convert.ToInt32(iptWriteBuffer.Text);
            this.sp_GageData.WriteTimeout = Convert.ToInt32(iptWriteTimeout.Text);
            this.sp_GageData.ReceivedBytesThreshold = Convert.ToInt32(iptReceiveBytes.Text);
            this.sp_GageData.ParityReplace = Convert.ToByte(iptParityReplace.Text);

            if (this.chkMultiLine.Checked)
            {
                txtReceiveData.Multiline = true;
            }
            else
            {
                txtReceiveData.Multiline = false;
            }

            txtReceiveData.Text = "";

            return;
        }
    }
}