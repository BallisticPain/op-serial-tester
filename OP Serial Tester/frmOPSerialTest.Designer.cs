﻿namespace OP_Serial_Tester
{
    partial class frmOPSerialTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOPSerialTest));
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabData = new System.Windows.Forms.TabPage();
            this.btnWriteData = new System.Windows.Forms.Button();
            this.txtWriteData = new System.Windows.Forms.TextBox();
            this.txtReceiveData = new System.Windows.Forms.TextBox();
            this.tabSetup = new System.Windows.Forms.TabPage();
            this.iptReceiveBytes = new System.Windows.Forms.TextBox();
            this.iptParityReplace = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.iptWriteTimeout = new System.Windows.Forms.TextBox();
            this.iptWriteBuffer = new System.Windows.Forms.TextBox();
            this.iptReadTimeout = new System.Windows.Forms.TextBox();
            this.iptReadBuffer = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.iptDTREnable = new System.Windows.Forms.CheckBox();
            this.iptRTSEnable = new System.Windows.Forms.CheckBox();
            this.iptNewLine = new System.Windows.Forms.TextBox();
            this.iptHandshake = new System.Windows.Forms.ComboBox();
            this.iptStopBits = new System.Windows.Forms.ComboBox();
            this.iptParity = new System.Windows.Forms.ComboBox();
            this.iptDataBits = new System.Windows.Forms.NumericUpDown();
            this.iptBaudRate = new System.Windows.Forms.TextBox();
            this.iptCOMPort = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnConnectSetup = new System.Windows.Forms.Button();
            this.chkMultiLine = new System.Windows.Forms.CheckBox();
            this.sp_GageData = new System.IO.Ports.SerialPort(this.components);
            this.tabMain.SuspendLayout();
            this.tabData.SuspendLayout();
            this.tabSetup.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMain.Controls.Add(this.tabData);
            this.tabMain.Controls.Add(this.tabSetup);
            this.tabMain.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(238, 295);
            this.tabMain.TabIndex = 0;
            // 
            // tabData
            // 
            this.tabData.Controls.Add(this.btnWriteData);
            this.tabData.Controls.Add(this.txtWriteData);
            this.tabData.Controls.Add(this.txtReceiveData);
            this.tabData.Location = new System.Drawing.Point(4, 20);
            this.tabData.Name = "tabData";
            this.tabData.Size = new System.Drawing.Size(230, 271);
            this.tabData.Text = "Data";
            // 
            // btnWriteData
            // 
            this.btnWriteData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWriteData.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.btnWriteData.Location = new System.Drawing.Point(154, 248);
            this.btnWriteData.Name = "btnWriteData";
            this.btnWriteData.Size = new System.Drawing.Size(73, 20);
            this.btnWriteData.TabIndex = 2;
            this.btnWriteData.Text = "Write (Send)";
            this.btnWriteData.Click += new System.EventHandler(this.btnWriteData_Click);
            // 
            // txtWriteData
            // 
            this.txtWriteData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWriteData.Location = new System.Drawing.Point(3, 245);
            this.txtWriteData.Name = "txtWriteData";
            this.txtWriteData.Size = new System.Drawing.Size(145, 23);
            this.txtWriteData.TabIndex = 1;
            // 
            // txtReceiveData
            // 
            this.txtReceiveData.AcceptsReturn = true;
            this.txtReceiveData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReceiveData.Location = new System.Drawing.Point(3, 3);
            this.txtReceiveData.Multiline = true;
            this.txtReceiveData.Name = "txtReceiveData";
            this.txtReceiveData.Size = new System.Drawing.Size(224, 236);
            this.txtReceiveData.TabIndex = 0;
            // 
            // tabSetup
            // 
            this.tabSetup.Controls.Add(this.iptReceiveBytes);
            this.tabSetup.Controls.Add(this.iptParityReplace);
            this.tabSetup.Controls.Add(this.label14);
            this.tabSetup.Controls.Add(this.label13);
            this.tabSetup.Controls.Add(this.iptWriteTimeout);
            this.tabSetup.Controls.Add(this.iptWriteBuffer);
            this.tabSetup.Controls.Add(this.iptReadTimeout);
            this.tabSetup.Controls.Add(this.iptReadBuffer);
            this.tabSetup.Controls.Add(this.label11);
            this.tabSetup.Controls.Add(this.label12);
            this.tabSetup.Controls.Add(this.label10);
            this.tabSetup.Controls.Add(this.label9);
            this.tabSetup.Controls.Add(this.iptDTREnable);
            this.tabSetup.Controls.Add(this.iptRTSEnable);
            this.tabSetup.Controls.Add(this.iptNewLine);
            this.tabSetup.Controls.Add(this.iptHandshake);
            this.tabSetup.Controls.Add(this.iptStopBits);
            this.tabSetup.Controls.Add(this.iptParity);
            this.tabSetup.Controls.Add(this.iptDataBits);
            this.tabSetup.Controls.Add(this.iptBaudRate);
            this.tabSetup.Controls.Add(this.iptCOMPort);
            this.tabSetup.Controls.Add(this.label8);
            this.tabSetup.Controls.Add(this.label7);
            this.tabSetup.Controls.Add(this.label6);
            this.tabSetup.Controls.Add(this.label5);
            this.tabSetup.Controls.Add(this.label4);
            this.tabSetup.Controls.Add(this.label3);
            this.tabSetup.Controls.Add(this.label2);
            this.tabSetup.Controls.Add(this.label1);
            this.tabSetup.Controls.Add(this.btnDisconnect);
            this.tabSetup.Controls.Add(this.btnConnectSetup);
            this.tabSetup.Controls.Add(this.chkMultiLine);
            this.tabSetup.Location = new System.Drawing.Point(4, 20);
            this.tabSetup.Name = "tabSetup";
            this.tabSetup.Size = new System.Drawing.Size(230, 271);
            this.tabSetup.Text = "Setup";
            // 
            // iptReceiveBytes
            // 
            this.iptReceiveBytes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.iptReceiveBytes.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptReceiveBytes.Location = new System.Drawing.Point(152, 170);
            this.iptReceiveBytes.Name = "iptReceiveBytes";
            this.iptReceiveBytes.Size = new System.Drawing.Size(63, 18);
            this.iptReceiveBytes.TabIndex = 80;
            this.iptReceiveBytes.Text = "1";
            // 
            // iptParityReplace
            // 
            this.iptParityReplace.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.iptParityReplace.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptParityReplace.Location = new System.Drawing.Point(152, 207);
            this.iptParityReplace.Name = "iptParityReplace";
            this.iptParityReplace.Size = new System.Drawing.Size(63, 18);
            this.iptParityReplace.TabIndex = 79;
            this.iptParityReplace.Text = "63";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label14.Location = new System.Drawing.Point(152, 156);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 14);
            this.label14.Text = "Receive Bytes";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label13.Location = new System.Drawing.Point(151, 193);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 14);
            this.label13.Text = "Parity Replace";
            // 
            // iptWriteTimeout
            // 
            this.iptWriteTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.iptWriteTimeout.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptWriteTimeout.Location = new System.Drawing.Point(152, 133);
            this.iptWriteTimeout.Name = "iptWriteTimeout";
            this.iptWriteTimeout.Size = new System.Drawing.Size(63, 18);
            this.iptWriteTimeout.TabIndex = 62;
            this.iptWriteTimeout.Text = "-1";
            // 
            // iptWriteBuffer
            // 
            this.iptWriteBuffer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.iptWriteBuffer.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptWriteBuffer.Location = new System.Drawing.Point(152, 96);
            this.iptWriteBuffer.Name = "iptWriteBuffer";
            this.iptWriteBuffer.Size = new System.Drawing.Size(63, 18);
            this.iptWriteBuffer.TabIndex = 61;
            this.iptWriteBuffer.Text = "2048";
            // 
            // iptReadTimeout
            // 
            this.iptReadTimeout.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.iptReadTimeout.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptReadTimeout.Location = new System.Drawing.Point(152, 59);
            this.iptReadTimeout.Name = "iptReadTimeout";
            this.iptReadTimeout.Size = new System.Drawing.Size(63, 18);
            this.iptReadTimeout.TabIndex = 48;
            this.iptReadTimeout.Text = "-1";
            // 
            // iptReadBuffer
            // 
            this.iptReadBuffer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.iptReadBuffer.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptReadBuffer.Location = new System.Drawing.Point(152, 22);
            this.iptReadBuffer.Name = "iptReadBuffer";
            this.iptReadBuffer.Size = new System.Drawing.Size(63, 18);
            this.iptReadBuffer.TabIndex = 47;
            this.iptReadBuffer.Text = "4096";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label11.Location = new System.Drawing.Point(152, 119);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 14);
            this.label11.Text = "Write Timeout";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label12.Location = new System.Drawing.Point(152, 82);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 14);
            this.label12.Text = "Write Buffer";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label10.Location = new System.Drawing.Point(152, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 14);
            this.label10.Text = "Read Timeout";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label9.Location = new System.Drawing.Point(152, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 14);
            this.label9.Text = "Read Buffer";
            // 
            // iptDTREnable
            // 
            this.iptDTREnable.Checked = true;
            this.iptDTREnable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.iptDTREnable.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptDTREnable.Location = new System.Drawing.Point(32, 176);
            this.iptDTREnable.Name = "iptDTREnable";
            this.iptDTREnable.Size = new System.Drawing.Size(89, 18);
            this.iptDTREnable.TabIndex = 38;
            this.iptDTREnable.Text = "DTR Enable";
            // 
            // iptRTSEnable
            // 
            this.iptRTSEnable.Checked = true;
            this.iptRTSEnable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.iptRTSEnable.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptRTSEnable.Location = new System.Drawing.Point(32, 200);
            this.iptRTSEnable.Name = "iptRTSEnable";
            this.iptRTSEnable.Size = new System.Drawing.Size(89, 19);
            this.iptRTSEnable.TabIndex = 37;
            this.iptRTSEnable.Text = "RTS Enable";
            // 
            // iptNewLine
            // 
            this.iptNewLine.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.iptNewLine.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptNewLine.Location = new System.Drawing.Point(66, 156);
            this.iptNewLine.Name = "iptNewLine";
            this.iptNewLine.Size = new System.Drawing.Size(66, 18);
            this.iptNewLine.TabIndex = 28;
            this.iptNewLine.Text = "CR";
            // 
            // iptHandshake
            // 
            this.iptHandshake.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.iptHandshake.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptHandshake.Items.Add("None");
            this.iptHandshake.Items.Add("XOnXOff");
            this.iptHandshake.Items.Add("RequestToSend");
            this.iptHandshake.Items.Add("RequestToSendXOnXOff");
            this.iptHandshake.Location = new System.Drawing.Point(66, 128);
            this.iptHandshake.Name = "iptHandshake";
            this.iptHandshake.Size = new System.Drawing.Size(66, 18);
            this.iptHandshake.TabIndex = 27;
            this.iptHandshake.Text = "None";
            // 
            // iptStopBits
            // 
            this.iptStopBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.iptStopBits.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptStopBits.Items.Add("None");
            this.iptStopBits.Items.Add("One");
            this.iptStopBits.Items.Add("OnePointFive");
            this.iptStopBits.Items.Add("Two");
            this.iptStopBits.Location = new System.Drawing.Point(66, 104);
            this.iptStopBits.Name = "iptStopBits";
            this.iptStopBits.Size = new System.Drawing.Size(66, 18);
            this.iptStopBits.TabIndex = 26;
            this.iptStopBits.Text = "One";
            // 
            // iptParity
            // 
            this.iptParity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.iptParity.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptParity.Items.Add("None");
            this.iptParity.Items.Add("Odd");
            this.iptParity.Items.Add("Even");
            this.iptParity.Items.Add("Mark");
            this.iptParity.Items.Add("Space");
            this.iptParity.Location = new System.Drawing.Point(66, 81);
            this.iptParity.Name = "iptParity";
            this.iptParity.Size = new System.Drawing.Size(66, 18);
            this.iptParity.TabIndex = 17;
            this.iptParity.Text = "None";
            // 
            // iptDataBits
            // 
            this.iptDataBits.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptDataBits.Location = new System.Drawing.Point(66, 57);
            this.iptDataBits.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.iptDataBits.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.iptDataBits.Name = "iptDataBits";
            this.iptDataBits.Size = new System.Drawing.Size(66, 19);
            this.iptDataBits.TabIndex = 16;
            this.iptDataBits.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // iptBaudRate
            // 
            this.iptBaudRate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.iptBaudRate.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptBaudRate.Location = new System.Drawing.Point(66, 34);
            this.iptBaudRate.Name = "iptBaudRate";
            this.iptBaudRate.Size = new System.Drawing.Size(66, 18);
            this.iptBaudRate.TabIndex = 15;
            this.iptBaudRate.Text = "9600";
            // 
            // iptCOMPort
            // 
            this.iptCOMPort.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.iptCOMPort.Location = new System.Drawing.Point(91, 5);
            this.iptCOMPort.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.iptCOMPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.iptCOMPort.Name = "iptCOMPort";
            this.iptCOMPort.Size = new System.Drawing.Size(41, 19);
            this.iptCOMPort.TabIndex = 14;
            this.iptCOMPort.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label8.Location = new System.Drawing.Point(66, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 14);
            this.label8.Text = "COM";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label7.Location = new System.Drawing.Point(3, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 14);
            this.label7.Text = "End of Line";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(3, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 14);
            this.label6.Text = "Port Name";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(3, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 14);
            this.label5.Text = "Handshake";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(3, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 14);
            this.label4.Text = "Stop Bits";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(3, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 14);
            this.label3.Text = "Parity";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(3, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 14);
            this.label2.Text = "Data Bits";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(3, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 14);
            this.label1.Text = "Baud Rate";
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDisconnect.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.btnDisconnect.Location = new System.Drawing.Point(109, 248);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(60, 20);
            this.btnDisconnect.TabIndex = 3;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnConnectSetup
            // 
            this.btnConnectSetup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConnectSetup.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.btnConnectSetup.Location = new System.Drawing.Point(175, 248);
            this.btnConnectSetup.Name = "btnConnectSetup";
            this.btnConnectSetup.Size = new System.Drawing.Size(46, 20);
            this.btnConnectSetup.TabIndex = 1;
            this.btnConnectSetup.Text = "Connect";
            this.btnConnectSetup.Click += new System.EventHandler(this.btnConnectSetup_Click);
            // 
            // chkMultiLine
            // 
            this.chkMultiLine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkMultiLine.Checked = true;
            this.chkMultiLine.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMultiLine.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.chkMultiLine.Location = new System.Drawing.Point(3, 248);
            this.chkMultiLine.Name = "chkMultiLine";
            this.chkMultiLine.Size = new System.Drawing.Size(100, 20);
            this.chkMultiLine.TabIndex = 0;
            this.chkMultiLine.Text = "Data Multi-Line?";
            // 
            // sp_GageData
            // 
            this.sp_GageData.DtrEnable = true;
            this.sp_GageData.NewLine = "\r";
            this.sp_GageData.RtsEnable = true;
            this.sp_GageData.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.sp_GageData_DataReceived);
            // 
            // frmOPSerialTest
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.tabMain);
            this.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmOPSerialTest";
            this.Text = "OP Serial Tester";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmOPSerialTest_Load);
            this.tabMain.ResumeLayout(false);
            this.tabData.ResumeLayout(false);
            this.tabSetup.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabData;
        private System.Windows.Forms.TabPage tabSetup;
        private System.Windows.Forms.TextBox txtReceiveData;
        private System.Windows.Forms.Button btnWriteData;
        private System.Windows.Forms.TextBox txtWriteData;
        private System.Windows.Forms.Button btnConnectSetup;
        private System.Windows.Forms.CheckBox chkMultiLine;
        private System.IO.Ports.SerialPort sp_GageData;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox iptBaudRate;
        private System.Windows.Forms.NumericUpDown iptCOMPort;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox iptParity;
        private System.Windows.Forms.NumericUpDown iptDataBits;
        private System.Windows.Forms.ComboBox iptStopBits;
        private System.Windows.Forms.TextBox iptNewLine;
        private System.Windows.Forms.ComboBox iptHandshake;
        private System.Windows.Forms.CheckBox iptDTREnable;
        private System.Windows.Forms.CheckBox iptRTSEnable;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox iptReadTimeout;
        private System.Windows.Forms.TextBox iptReadBuffer;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox iptWriteTimeout;
        private System.Windows.Forms.TextBox iptWriteBuffer;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox iptReceiveBytes;
        private System.Windows.Forms.TextBox iptParityReplace;
    }
}

